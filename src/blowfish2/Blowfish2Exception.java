/*  
 *   This file is part of Blowfish.
 * 
 *   http://www.carrol.biz/olafrv/blowfish.html
 *
 *   Copyright (C) Dec 23th, 2009 - Olaf Reitmaier Veracierta <olafrv@gmail.com>
 *
 *   Blowfish is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Blowfish is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Blowfish.  If not, see <http://www.gnu.org/licenses/>.
 */

package blowfish2;

public class Blowfish2Exception extends Exception{
	public Blowfish2Exception(String description){
		super(description);
	}
}
